import {IFilterField, IOperators, Language, Filter, IField} from "timeseriesfilter"
import {isRanges, toHighchartRanges} from "./utils";
import express from "express"
import path from "path"

const fieldISOTime: IFilterField = {
    key: "I",
    base: /\d{4}-\d{2}-\d{2}T(\d{2})?:?(\d{2})?:?(\d{2})?\.?(\d{1,3})?Z?/,
    wildcards: /(\d{4}|\*{1,4})-(\d{2}|\*{1,2})-(\d{2}|\*{1,2})T(\d{2})?:?(\d{2})?:?(\d{2})?\.?(\d{1,3})?Z?/,
    groups: /(\d{4})-(\d{2})-(\d{2})T(\d{2})?:?(\d{2})?:?(\d{2})?\.?(\d)?(\d)?(\d)?Z?/,
    groupsWildcards: /(\d{4}|\*{1,4})-(\d{2}|\*{1,2})-(\d{2}|\*{1,2})T(\d{2})?:?(\d{2})?:?(\d{2})?\.?(\d)?(\d)?(\d)?Z?/
}
const fieldTime: IFilterField = {
    key: "T",
    base: /(\d{1,2})?:?(\d{1,2})?:?(\d{1,2})?\.?(\d{1,3})?/,
    wildcards: /(\d{1,2}|\*{1,2})?:?(\d{1,2}|\*{1,2})?:?(\d{1,2}|\*{1,2})?\.?(\d{1,3}|\*{1,3})/,
    groups: /(\d)?(\d)?:?(\d)?(\d)?:?(\d)?(\d)?\.?(\d)?(\d)?(\d)?/,
    groupsWildcards: /([\d*])?([\d*])?:?([\d*])?([\d*])?:?([\d*])?([\d*])?\.?([\d*])?([\d*])?([\d*])?/
}

const operators: IOperators = {
    is: /=/,
    not: /!/,
    regIs: /~/,
    regNot: /\^/,
    and: /&/,
    or: /\|/,
    list: /,/,
    rangeInc: /--/,
    rangeLInc: />>/,
    rangeRInc: /<</,
    rangeNonInc: /__/,
    blockOpen: /\(/,
    blockClose: /\)/,
}

const language = new Language([fieldISOTime, fieldTime], operators)
const filter = new Filter(language)

const app = express()
const port: number = 8080

app.use(express.static(__dirname))
app.set("views", path.join(__dirname, "views"))
app.set("view engine", "ejs")

const fields: Map<string, IField> = new Map<string, IField>([
    ["I", {field: "ISO_TIME", join: ["-", "-", "T", ":", ":", ".", ""], zero: "0", type: "str"} as IField],
    ["T", {field: "TIME", join: ["", ":", "", ":", "", ".", ""], zero: "0", type: "str"} as IField],
])

app.get("/", (req: express.Request, res: express.Response) => {
    console.log(`load: ${(new Date()).toISOString()}`)

    const input: string = (req.query.input || "")
        .toString()
        .trim()
    console.log(input)

    let convertedString = ""
    let querySQL = ""
    let queryMongo = ""
    let chartRanges = ""

    if (input.length > 0) {
        filter.fromString(input)
        convertedString = filter.toString()

        if (filter.parsedFilters.length) {
            querySQL = filter.toQuery("sql", fields)
            queryMongo = filter.toQuery("mongo", fields)

            if (isRanges(filter.parsedFilters)) {
                chartRanges = toHighchartRanges(filter.parsedFilters, fields).join(",")
            }
        }

        console.log(JSON.stringify(filter.parsedFilters))
        console.log(convertedString)
        console.log(querySQL)
        console.log(queryMongo)
        console.log(chartRanges)
    }

    console.log()

    res.render("index", {convertedString, querySQL, queryMongo, chartRanges, input})
})

console.log("app start")
app.listen(port)
