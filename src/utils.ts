import {IField, Types} from "timeseriesfilter";
import {intersperse} from "timeseriesfilter/dist/utils";

export function isRanges(filtersList: Types.FiltersList): boolean {
    return filtersList
        .map((item) =>
            item.is(Types.Or) ||
            (
                item.is(Types.FilterItem) &&
                (
                    (item as Types.FilterItem).type.is(Types.RangeInc) ||
                    (item as Types.FilterItem).type.is(Types.RangeLInc) ||
                    (item as Types.FilterItem).type.is(Types.RangeRInc) ||
                    (item as Types.FilterItem).type.is(Types.RangeNonInc)
                )
            ))
        .every((v) => v === true)
}

function randomHexNumbers(length: number, max: number): string {
    const nums: number[] = []

    for (let i = 0; i < length; i++) nums.push(Math.floor(Math.random() * (max + 1)))

    return nums
        .map((ns) => ns.toString(16))
        .join("")
}

function joinValues(values: string[][], field: IField): string[] {
    return values
        .map((v) =>
            v.map((vj) =>
                vj.replace("*", field.zero as string)
            ))
        .map((v) => intersperse(v, field.join) as string[])
        .map((v) => v.join(""))
}

export function toHighchartRanges(filtersList: Types.FiltersList, fields: Map<string, IField>): string[] {
    return filtersList
        .filter((item) => item.is(Types.FilterItem))
        .map((range: Types.FilterItem) => joinValues(range.values, fields.get(range.field)))
        .map((values) => [randomHexNumbers(3, 255), values[0], values[1]].join(","))
}