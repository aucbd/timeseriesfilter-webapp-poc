# TimeSeriesFilter - Webapp Proof of Concept

## Syntax

`[Filter 1][Logic Operator][Filter 2][...][Logic Operator][Filter N]`

Filters and logic can be separated and nested with blocks:

`[Block Open Operator][Filters...][Block Close Operator][Logic Operator][Block Open Operator][Filters...][Block Close Operator][...]`

There are two types of filters: range and list

* Range <br/>
`[Field][Equality][Range A][Range Operator][Range B]`
* List <br/>
`[Field][Equality][List 1][List Operator][List 2][List Operator][List 3][...][List Operator][List N]`

### Fields

* `I` Time <br/>
ISO-formatted time string in the format `\d{4}-\d{2}-\d{2}T\d{2}?:?\d{2}?:?\d{2}?\.?\d{1,3}?Z?` `YYYY-MM-DDThh:mm:ss.sssZ` <br/>
The date elements and the `T` separator are mandatory, the time elements can be omitted and will be considered as either 0 or wildcards `*`. Date elements support wildcards.
* `T` Time <br/>
Time string in the format `(\d{1,2})?:?(\d{1,2})?:?(\d{1,2})?\.?(\d{1,3})?` `HH:MM:SS.sss` <br/>
Any element can be omitted and will be considered as either 0 or wildcards `*`. Wildcards are supported for each digit.

### Operators

#### Equality

* Is: `=`
* Is not: `!`
* Is regex: `~`
* Is not regex: `^`

#### Ranges

* Range inclusive [A, B]: `--`
* Range left-inclusive [A, B): `>>`
* Range right-inclusive (A, B]: `<<`
* Range non-inclusive (A, B): `__`

#### List

* List separator: `,`

#### Logic

* And: `&`
* Or: `|`

#### Blocks

* Block open: `(`
* Block close `)`

## Example

| Filter Command | Meaning |
| :--- | :--- |
| `I=2020-05-01T>>2020-06-01T` | Inside range [2020-05-01T00:00:00.000, 2020-06-01T00:00:00.000) |
| `I=2020-01-31T23:59:59__2020-03-01T|I=2020-05-01T>>2020-06-01T` | Inside range (2020-01-31T23:59:59.000, 2020-03-01T00:00:00.000) or Range [2020-05-01T00:00:00.000, 2020-06-01T00:00:00.000) |
| `I~2020-05-01T,2020-05-02T` | Either 2020-05-01T\*\*:\*\*:\*\*.\*\*\* or 2020-05-02T\*\*:\*\*:\*\*.\*\*\* |
| `I!2020-05-01T>>2020-06-01T` | Not in range [2020-05-01T00:00:00.000, 2020-06-01T00:00:00.000) |
| `T=10>>16` | Time inside range [10:00:00.000, 16:00:00.000) |
| `T=10>>16&I~2020-05-*T` | Time in range [10:00:00.000, 16:00:00.000) and date in regex 2020-05-* |
