module.exports = {
    root: true,
    parser: '@typescript-eslint/parser',
    parserOptions: {
        tsconfigRootDir: __dirname,
        project: ['tsconfig.json'],
    },
    plugins: [
        '@typescript-eslint',
    ],
    ignorePatterns: [
        ".eslintrc.js",
        "node_modules/*",
        "dist/*",
        "tools/*"
    ],
    extends: [
        'eslint:recommended',
        'plugin:@typescript-eslint/recommended',
        'plugin:@typescript-eslint/recommended-requiring-type-checking',
    ],
    rules: {
        "@typescript-eslint/no-inferrable-types": 0,
    }
}